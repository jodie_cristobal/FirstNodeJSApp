const express = require('express');
const app = express();
const mysql = require('mysql');
const bodyParser = require('body-parser');

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

// list all items
app.get('/', (req, res) => {
	// create connection
	var connection = mysql.createConnection({
		// properties
		host: 'localhost',
		user: 'root',
		password: ''
	});

	connection.connect((error) => {
		// callback
		if(error) throw error;
		console.log('Connected to mysql');

	});

	// create db and table item if not exists
	let sql = 'CREATE DATABASE IF NOT EXISTS nodejsexam';

	connection.query(sql, (error, result) => {
		if(error) throw error;
		console.log('Database Created');
	});

	// recreate connection using created database
	connection = mysql.createConnection({
		// properties
		host: 'localhost',
		user: 'root',
		password: '',
		database: 'nodejsexam'
	});

	connection.connect((error) => {
		// callback
		if(error) throw error;
		console.log('Connected to DB');

	});

	sql = 'CREATE TABLE IF NOT EXISTS item (id INT(3) UNSIGNED AUTO_INCREMENT, name VARCHAR(255) NOT NULL, quantity INT(3) NOT NULL DEFAULT 0, amount FLOAT(6, 2) NOT NULL DEFAULT 0.0, PRIMARY KEY(id))';

	connection.query(sql, (error, result) => {
		if(error) throw error;
		console.log('Item table created');
	});

	// load view using ejs
	app.set('view engine', 'ejs');

	sql = 'SELECT * FROM item';

	connection.query(sql, (err, results) => {
		if(err) throw err;
		res.render('list', {
			title: 'View items',
			data: results
		});
	});
});



// view add form
app.get('/add', (req, res) => {

	app.set('view engine', 'ejs');

	res.render('add', {
		title: 'Add Item',
		name: '',
		quantity: '',
		amount: ''
	});
});

// insert new item
app.post('/add', (req, res) => {
	// create connection
	connection = mysql.createConnection({
		host: 'localhost',
		user: 'root',
		password: '',
		database: 'nodejsexam'
	});

	var item = {
		name: req.body.name,
		quantity: req.body.quantity,
		amount: req.body.amount,
	}

	sql = 'INSERT INTO item SET ?';

	connection.query(sql, item, (err, result) => {
		if(err) throw err;
		res.redirect('/');
	});
});

// get individual item
app.get('/update/:id', (req, res) => {

	// create connection
	connection = mysql.createConnection({
		host: 'localhost',
		user: 'root',
		password: '',
		database: 'nodejsexam'
	});

	app.set('view engine', 'ejs');

	sql = `SELECT * FROM item WHERE id=${req.params.id}`;
	connection.query(sql, (err, result, fields) => {
		if(err) throw err;

		res.render('edit', {
			title: 'Edit Item',
			id: result[0].id,
			name: result[0].name,
			quantity: result[0].quantity,
			amount: result[0].amount
		});
	});
});

// update individual item
app.post('/update/:id', (req, res) => {
	// create connection
	connection = mysql.createConnection({
		host: 'localhost',
		user: 'root',
		password: '',
		database: 'nodejsexam'
	});

	var item = {
		name: req.body.name,
		quantity: req.body.quantity,
		amount: req.body.amount,
	}

	sql = `UPDATE item SET ? WHERE id=${req.params.id}`;

	connection.query(sql, item, (err, result) => {
		if(err) throw err;
		res.redirect('/');
	});
});

// delete item
app.get('/delete/:id', (req, res) => {
	let sql = `DELETE FROM item WHERE id = ${req.params.id}`;

	connection = mysql.createConnection({
		// properties
		host: 'localhost',
		user: 'root',
		password: '',
		database: 'nodejsexam'
	});

	connection.query(sql, (err, result) => {
		if(err) throw err;
		res.redirect('/');
	});
});

// listen to port 3000
app.listen('3000', () => {
	console.log('Server started on port 3000' );
});